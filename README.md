# Requirements
- hosts an IDE for collaborative programming work
- able to support at least 2 concurrent users
- users can work together: locked to viewing the same thing and the same time, and both
  can control. Can be "screen share" style or "google docs" style
- users can work separately: more like separate SSH sessions
- can run any toolchain: nodejs, python, etc
- choice of IDE: vim, vscode
- terminal is accessible
- access via browser for the whole IDE
- if "screen share" style:
    - minimal latency local editor can be connected
    - any web server is separately exposed, so it can be hit directly
    - web browser runs server-side so "inspect" and "debug" activity is also shared


# Prototyping
Build on top of `lscr.io/linuxserver/code-server`
https://github.com/linuxserver/docker-code-server

```
docker run -d \
  --name=code-server \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e PASSWORD=password `#optional` \
  -e HASHED_PASSWORD= `#optional` \
  -e SUDO_PASSWORD=password `#optional` \
  -e SUDO_PASSWORD_HASH= `#optional` \
  -e PROXY_DOMAIN=code-server.my.domain `#optional` \
  -e DEFAULT_WORKSPACE=/config/workspace `#optional` \
  -p 8443:8443 \
  lscr.io/linuxserver/code-server:latest
```

# packages to add
```bash
sudo apt update
sudo apt install -y \
  vim \
  wget
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
cat <<"HEREDOC" >> ~/.bashrc
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
HEREDOC
```

# TODO
- make the ttyd creds the same as code-server
- might also want tty-share for a live-share view, or just use byobu/tmux/screen. Getting
  errors with byobu/tmux
- can we use live-share for vscode?

# Features
- code-server does auto-magic port forwarding, just have to login to access


can run https://github.com/jlesage/docker-firefox
```
docker run --rm -it \
  --device /dev/snd
  -p 5800:5800 \
  jlesage/firefox
```

note: sound still didn't work.

Then connect with:
```
chromium --noerrdialogs --disable-infobars --kiosk http://localhost:5800/
```
...and we get a fullscreen client and C-w, C-t work

ASK try having a full X desktop

# Reviews of other platforms
## codesandbox.io
- has "live sessions" feature
    - It works a little bit
    - You can follow avatars and it takes you into files (but not terminals) and follows
      line focus. It messes up the default view for markdown files
    - lots of little things don't work, like newly connected users don't have their avatar
      show up, so you have to reload the page to see them
    - web browsing is not sync'd
    - it's more of a "work on the same code base at the same time" feature than a "let me
      show you what I'm seeing" feature
- typing in a terminal is slow, probably hosted in america
- have to use crusty cloud IDE, no vim bindings
- generally unhappy about the experience after having to do it
